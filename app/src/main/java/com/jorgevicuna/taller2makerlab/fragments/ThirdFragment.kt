package com.jorgevicuna.taller2makerlab.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jorgevicuna.taller2makerlab.R
import kotlinx.android.synthetic.main.fragment_first.*


class ThirdFragment:Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first, container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        title.text="Third Fragmnet"
        imageView.setImageResource(R.mipmap.ic_launcher)
    }
}