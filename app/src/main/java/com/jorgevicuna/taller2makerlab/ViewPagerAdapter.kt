package com.jorgevicuna.taller2makerlab

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

import com.jorgevicuna.taller2makerlab.fragments.FirstFragment
import com.jorgevicuna.taller2makerlab.fragments.SecondFragment
import com.jorgevicuna.taller2makerlab.fragments.ThirdFragment


class ViewPagerAdapter internal constructor(fm: FragmentManager): FragmentPagerAdapter(fm) {

    private val COUNT=3

    override fun getItem(position: Int): Fragment {
        var fragment:Fragment? = null
        when(position){
            0 -> fragment = FirstFragment()
            1 -> fragment = SecondFragment()
            2 -> fragment = ThirdFragment()
        }
        return fragment !!
    }

    override fun getCount(): Int {
         return COUNT
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "Tab" + (position + 1)
    }
}