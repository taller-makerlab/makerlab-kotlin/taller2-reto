package com.jorgevicuna.taller2makerlab.activities

data class Coach(
    val name: String,
    val universidad: String,
    val carrera: String,
    val edad: String

)

