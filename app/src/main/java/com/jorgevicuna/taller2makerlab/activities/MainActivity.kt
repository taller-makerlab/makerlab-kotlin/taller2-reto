package com.jorgevicuna.taller2makerlab.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.jorgevicuna.taller2makerlab.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        button.setOnClickListener {
            val intent =Intent(this,IntentsActivity::class.java)
            startActivity(intent)
        }
        button2.setOnClickListener {
            val intent =Intent(this,ViewPagerActivity::class.java)
            startActivity(intent)
        }
        button3.setOnClickListener {
            val intent = Intent(this,RecyclerViewActivity::class.java)
            intent.putExtra("TITULO","KOTLIN PRUEBA")
            startActivity(intent)
            //textView.text = "Hola amigos"
            //Toast.makeText(applicationContext,"Hola amigos",Toast.LENGTH_SHORT).show()


        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //Inflate the menu: this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.action_maker, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem)=when(item.itemId) {
        R.id.action_intents ->{
            val intent =Intent(this,IntentsActivity::class.java)
            startActivity(intent)
            Toast.makeText(applicationContext,"Intents Implícitos",Toast.LENGTH_SHORT).show()
            true
        }
        R.id.action_viewpager ->{
            val intent =Intent(this,ViewPagerActivity::class.java)
            startActivity(intent)
            Toast.makeText(applicationContext,"ViewPager",Toast.LENGTH_SHORT).show()
            true
        }
        R.id.action_recyclerview ->{
            val intent = Intent(this,RecyclerViewActivity::class.java)
            intent.putExtra("TITULO","KOTLIN PRUEBA")
            startActivity(intent)
            Toast.makeText(applicationContext,"RecyclerView",Toast.LENGTH_SHORT).show()
            true
        }
        else ->{
            super.onOptionsItemSelected(item)
        }

    }
}
