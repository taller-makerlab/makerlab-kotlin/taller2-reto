package com.jorgevicuna.taller2makerlab.activities

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.jorgevicuna.taller2makerlab.R
import com.jorgevicuna.taller2makerlab.ViewPagerAdapter
import com.tuann.floatingactionbuttonexpandable.FloatingActionButtonExpandable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_viewpager.*

class ViewPagerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewpager)
        setSupportActionBar(toolbar)

        val fab = findViewById<FloatingActionButtonExpandable>(R.id.fab)
        fab.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        val viewPagerAdapter= ViewPagerAdapter(
            supportFragmentManager
        )
        viewPager.adapter=viewPagerAdapter
    }

}