package com.jorgevicuna.taller2makerlab.activities

import android.content.Intent
import com.jorgevicuna.taller2makerlab.R
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_recyclerview.*
import com.tuann.floatingactionbuttonexpandable.FloatingActionButtonExpandable

class RecyclerViewActivity :AppCompatActivity() {

    private val coachAdapter = CoachAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recyclerview)
        val fab = findViewById<FloatingActionButtonExpandable>(R.id.fab)
        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val title = intent.getStringExtra("TITULO")
        textView2.text = title
        setRecyclerView()

        fab.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    fun setRecyclerView() {
        rvList.layoutManager = LinearLayoutManager(applicationContext)
        rvList.adapter = coachAdapter
        coachAdapter.setListCoach(getListCoach())

    }

    fun getListCoach(): List<Coach> {

        val list = mutableListOf<Coach>()
        list.add(Coach("Jorge", "UPN", "Ing.Electrónica", "24"))
        list.add(Coach("Jorge", "UPN", "Ing.Electrónica", "24"))
        list.add(Coach("Jorge", "UPN", "Ing.Electrónica", "24"))
        list.add(Coach("Jorge", "UPN", "Ing.Electrónica", "24"))
        list.add(Coach("Jorge", "UPN", "Ing.Electrónica", "24"))
        list.add(Coach("Jorge", "UPN", "Ing.Electrónica", "24"))
        return list

    }

    override fun onSupportNavigateUp(): Boolean {

        onBackPressed()
        return true

    }
}